# custom_dic_maker.py for nvdajp_jtalk
# -*- coding: utf-8 -*-
# since 2011-01-19 by Takuya Nishimoto

from __future__ import unicode_literals
OUT_FILE = 'nvdajp-custom-dic.csv'

import sys
import re
import os
from os import path

jdic = [
		# first item should use fullshape(zenkaku) charactors
		['読み込み中',	'ヨミコミチュー',		"2/6"],
		#['一行', 		'イチギョー',			"2/4"],
		#['１行', 		'イチギョー',			"2/4"],
		#['１行下', 	'イチギョーシタ',		"2/6"],
		#['１行上', 	'イチギョーウエ',		"2/6"],
		#['２行', 		'ニギョー',			"1/3"],
		#['３行', 		'サンギョー',			"1/4"],
		['行',	'ギョー',			"1/2", 	1000, "名詞,接尾,助数詞,*,*,*"],
		['行上',	'ギョーウエ',			"1/4", 	1000, "名詞,接尾,助数詞,*,*,*"],
		['行下',	'ギョーシタ',			"1/4", 	1000, "名詞,接尾,助数詞,*,*,*"],
		['５０音順', 	'ゴジューオンジュン',	"0/7", None, None, '50オンジュン'],
		['空行', 		'クーギョー',			"0/4"],
		['行末', 		'ギョーマツ',			"0/4"],
		['複数行', 	'フクスーギョー',		"3/6"],
		['現在行', 	'ゲンザイギョー',		"3/6"],
		['最上行', 	'サイジョーギョー',	"3/6"],
		['行操作',		'ギョーソーサ',		"1/5"],
		['誤判定',		'ゴハンテイ',		    "2/5"],
		['表計算',		'ヒョーケイサン',		"3/5"],
		['空要素',		'カラヨーソ',			"3/5"],
		['拡張子',		'カクチョーシ',		"3/5"],
		['親オブジェクト',		'オヤオブジェクト',		"3/7"],
		['小文字',		'コモジ',		"0/3"],
		['大文字',		'オーモジ',	"0/4"],
		['ニコ生',		'ニコナマ',			"0/4"],
		['スリーマイル島原発',	'スリーマイルトーゲンパツ'],

		['孫正義', 	'ソンマサヨシ', 		"4/6", None, None, 'ソン マサヨシ'],
		['池田信夫',	'イケダノブオ',		"0/6"],
		['方々',		'カタガタ',			"2/4"],
		['当分の間',	'トーブンノアイダ',	"0/8"],
		['中通り',		'ナカドーリ',			"3/5"],
		['中',			'チュー',				"1/2", 5000],
		['中の人',		'ナカノヒト',			"1/5"],
		['中程度',		'チューテード',		"3/5"],
		['各基',		'カクキ',				"1/3"],
		['高',			'コー',				"1/2", 5000],
		['県立高',		'ケンリツコー',		"0/6"],
		['業務',		'ギョーム',			"1/3"],
		['値',			'アタイ',				"0/3"],
		['２４時間', 	'ニジューヨジカン'		"1/7"],
		['明朝',		'ミンチョー',			"1/4"],
		['障がい',		'ショーガイ',			"0/4"],
		['蓮舫', 		'レンホー',			"1/4"],
		['既読', 		'キドク',				"0/3"],
		['新家', 		'シンケ',				"1/3"],
		['大嘘', 		'オーウソ',			"0/4"],
		['１人', 		'ヒトリ',				"2/3"],
		['一人ひとり', 'ヒトリヒトリ',		"0/6"],
		['日中', 		'ニッチュー',			"3/4"],
		['次',			'ツギ',				"2/2", 5000],
		['他人事',		'タニンゴト',			"0/5"],
		['セブン―イレブン', 	'セブンイレブン',				"5/7"],
		['東国原',		'ヒガシコクバル',		"5/7"],
		['中越',		'チューエツ',			"1/4"],
		['発災',		'ハッサイ',			"0/4"],
		['その上',		'ソノウエ',			"0/4"],
		['時期',		'ジキ',				"1/2"],
		['扱い',		'アツカイ',			"0/4"],
		['停波',		'テイハ',				"0/3"],
		['建屋',		'タテヤ',				"2/3"],
		['なう',		'ナウ',				"1/2"],
		['被り',		'カブリ',				"0/3"],
		['寺田寅彦',	'テラダトラヒコ',		"0/7"],
		['橋下',	'ハシモト',		"0/4"],
		['フレッツ光',	'フレッツヒカリ',		"2/7"],
		['選択行',		'センタクギョー',		"0/6"],
		['ベクレル',	'ベクレル',			"1/4", 	1000, "名詞,接尾,助数詞,*,*,*"],
		['三毛猫',		'ミケネコ',			"0/4"],
		['数多く',		'カズオオク',			"1/5"],
		['繁体字',		'ハンタイジ',			"3/5"],
		['上矢印',		'ウエヤジルシ',		"4/6"],
		['下矢印',		'シタヤジルシ',		"4/6"],
		['大見出し',	'オオミダシ',	        "3/5"],
		['前景色',	'ゼンケイショク',	        "3/6"],
		['八ッ場',	'ヤンバ',	        "0/3"],
		['梅雨前線', 'バイウゼンセン', "4/7", None, None, 'バイウ ゼンセン'],
		['１都５県', 'イットゴケン'],
		['１都６県', 'イットロッケン'],
		['昔々', 'ムカシムカシ', "0/6", None, None, 'ムカシ ムカシ'],
		['材販', 'ザイハン', "0/4"],
		['盲ろう者', 'モーローシャ', "3/5"],
		['えき', 'エキ', "1/2"],
		['はは', 'ハハ', "1/2"],
		['万国旗', 'バンコクキ', "3/5"],
		['多角形', 'タカクケイ', "2/5"],
		['高脂血症', 'コーシケツショー', "0/7"],
		['買うた', 'コータ', "1/3"],
		['縫うた', 'ヌータ', "0/3"],
		['透徹る', 'スキトオル', "3/5"],
		['八日', 'ヨーカ', "0/3"],
		['何百', 'ナンビャク', "1/4"],
		['十日', 'トオカ', "0/3"],
		['ちゅうりっぷ', 'チューリップ', "1/5"],
		['きゃりーぱみゅぱみゅ', 'キャリーパミュパミュ', "4/7"],
		['１月', 'イチガツ', '2/4', None, None, '1ガツ'],
		['２月', 'ニガツ', '1/3', None, None, '2ガツ'],
		['３月', 'サンガツ', '1/4', None, None, '3ガツ'],
		['４月', 'シガツ', '1/3', None, None, '4ガツ'],
		['５月', 'ゴガツ', '1/3', None, None, '5ガツ'],
		['６月', 'ロクガツ', '2/4', None, None, '6ガツ'],
		['７月', 'シチガツ', '2/4', None, None, '7ガツ'],
		['８月', 'ハチガツ', '2/4', None, None, '8ガツ'],
		['９月', 'クガツ', '1/3', None, None, '9ガツ'],
		['１０月', 'ジューガツ', '1/4', None, None, '10ガツ'],
		['１１月', 'ジューイチガツ', '4/6', None, None, '11ガツ'],
		['１２月', 'ジューニガツ', '3/5', None, None, '12ガツ'],
		['為おおせる', 'シオオセル', '4/5'],
		['砂利道', 'ジャリミチ', '2/4'],
		['少しずつ', 'スコシズツ', '4/5'],
		['まづ', 'マズ', '1/2'],
		['一つづつ', 'ヒトツズツ', '4/5'],
		['大きう', 'オオキュー', '1/4'],
		['うれしう', 'ウレシュー', '2/4'],
		['みづうみ', 'ミズウミ', '2/4'], 
		['もみぢ', 'モミジ', '1/3'], 
		['ヴァイオリン', 'バイオリン' , '0/5', None, None, 'ヴァイオリン'], 
		['ヴィタミン', 'ビタミン', '1/4', None, None, 'ヴィタミン'], 
		['ラヂオ', 'ラジオ', '1/3'], 
		['ヂャケット', 'ジャケット', '1/4'], 
		['ウヰスキー', 'ウイスキー', '1/5'], 
		['スヰフト', 'スイフト', '1/4'], 
		['ヱルテル', 'ウェルテル', '1/4'], 
		['ヲルポール', 'ウォルポール', '1/5'], 
		['ヘリコプタア', 'ヘリコプター', '1/6'], 
		['ちゅうりっぷ', 'チューリップ', '1/5'], 
		['おみやぁさん', 'オミャアサン', '2/5'], 
		['先生ぇさまぁ', 'センセエサマア', '0/7'], 
		['おとゥ', 'オトー', '2/3'], 
		['ヂェスチャー', 'ジェスチャー', '1/3'], 
		['ヒァーッ', 'ヒャーッ', '1/3'], 
		['東井', 'トーイ', '1/3'], 
		['みやこをどり', 'ミヤコオドリ', '4/6', None, None, 'ミヤコ オドリ'],
		['をりがみ', 'オリガミ', '2/4'],
		['キャレット', 'キャレット', '1/4'],
		['ヱビスビール', 'エビス ビール', '4/6', None, None, 'エビス ビール'],
		['十数人', 'ジュースーニン', '3/6', None, None, '10スーニン'],
		['いらっしゃい', 'イラッシャイ', '2/5'],
		['ごめんください', 'ゴメンクダサイ', '0/7', None, None, 'ゴメン クダサイ'],
		['おはようございます', 'オハヨーゴザイマス', '0/9', None, None, 'オハヨー ゴザイマス'],
		['嘘みたい', 'ウソミタイ', '1/5'],
		['満遍', 'マンベン', '0/4'],
		['形なし', 'カタナシ', '0/4'],
		['わかりっこ', 'ワカリッコ', '3/5'],
		['言わしむれば', 'イワシムレバ', '4/6'],
		['一人', 'ヒトリ', '2/3'],
		['二人', 'フタリ', '0/3'],
		['於て', 'オイテ', '1/3'],
		['この期', 'コノゴ', '0/3'],
		['その節', 'ソノセツ', '3/4'],
		['二十日', 'ハツカ', '0/3'],
		['二十歳', 'ハタチ', '1/3'],
		['３泊４日', 'サンパクヨッカ', '1/7', None, None, '3パク ヨッカ'],
		['二百十日', 'ニヒャクトーカ', '0/6', None, None, '2ヒャク トオカ'],
		['一日', 'ツイタチ', '0/4'],
		['十日', 'トーカ', '0/3', 100, None, 'トオカ'],
		['十四日', 'ジューヨッカ', '1/5', None, None, '14カ'],
		['二十四日', 'ニジューヨッカ', '1/6', None, None, '24カ'],
		['三三七拍子', 'サンサンナナビョーシ', '0/9', None, None, '3⠼3⠼7ビョーシ'],
		['三十三間堂', 'サンジューサンゲンドー', '1/10', None, None, '33ゲンドー'],
		['フレンドシップ', 'フレンドシップ', '5/7'],
		['我等', 'ワレラ', '1/3'],
		['相たずさえる', 'アイタズサエル', '1/7'],
		['各方面', 'カクホーメン', '1/6', None, None, 'カク ホーメン'],
		['旧陸軍', 'キューリクグン', '1/6', None, None, 'キュー リクグン'],
		['山や川', 'ヤマヤカワ', '2/5', None, None, 'ヤマヤ カワ'],

		['きゃ', 'キャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['きゅ', 'キュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['きょ', 'キョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['しゃ', 'シャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['しゅ', 'シュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['しょ', 'ショ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ちゃ', 'チャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ちゅ', 'チュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ちょ', 'チョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['にゃ', 'ニャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['にゅ', 'ニュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['にょ', 'ニョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ひゃ', 'ヒャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ひゅ', 'ヒュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ひょ', 'ヒョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['みゃ', 'ミャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['みゅ', 'ミュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['みょ', 'ミョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['りゃ', 'リャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['りゅ', 'リュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['りょ', 'リョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['くゎ', 'クワ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],

		['ぎゅ', 'ギュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぎょ', 'ギョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['じゃ', 'ジャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['じゅ', 'ジュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['じょ', 'ジョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぢゃ', 'ジャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぢゅ', 'ジュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぢょ', 'ジョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['びゃ', 'ビャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['びゅ', 'ビュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['びょ', 'ビョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぴゃ', 'ピャ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぴゅ', 'ピュ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぴょ', 'ピョ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		['ぐゎ', 'グワ',		"1/1", 		15000,		"記号,一般,*,*,*,*"],
		
	]

def alpha2mb(s):
	# 'abc' -> 'ａｂｃ'
	import string
	from_table = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
	to_table = 'ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ'
	result = ''
	for ch in s:
		pos = string.find(from_table, ch)
		if pos >= 0:
			result += to_table[pos]
	return result

def make_dic(CODE, THISDIR):
	with open(path.join(THISDIR, OUT_FILE), "w") as file:
		## jdic
		for i in jdic:
			k = i[0]
			k1 = k
			y = i[1]
			mora_count = len(y)
			pros = "0/%d" % mora_count
			cost = 1000
			pos = "名詞,一般,*,*,*,*"
			brl = None
			if len(i) >= 3: pros = i[2]
			if len(i) >= 4 and i[3]: cost = i[3]
			if len(i) >= 5 and i[4]: pos  = i[4]
			if len(i) >= 6: brl  = i[5]
			# 表層形,左文脈ID,右文脈ID,コスト,品詞,品詞細分類1,品詞細分類2,品詞細分類3,活用形,活用型,原形,読み,発音
			s = "%s,-1,-1,%d,%s,%s,%s,%s,%s,C0" % (k1,cost,pos,k1,y,y,pros)
			if brl:
				s += "," + brl
			s += "\n"
			file.write(s.encode(CODE))

if __name__ == '__main__':
	make_dic()
